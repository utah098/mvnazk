//
//  ApplicationAssemblyImpl.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit

final class ApplicationAssemblyImpl: ApplicationAssembly {

    let window: UIWindow
    var dbMigationManager: DatabaseMigrationManager
    var navigationLocator: ApplicationNavigatorLocator
    let navigationController: UINavigationController

    var applicationNavigator: ApplicationNavigator {
      return navigationLocator.shared
    }

    init(window: UIWindow, dataBaseMigrationManager: DatabaseMigrationManager, navigationLocator: ApplicationNavigatorLocator) {
        self.navigationController = UINavigationController()
        self.dbMigationManager = dataBaseMigrationManager
        self.navigationLocator = navigationLocator
        self.window = window
        setupApplicationNavigator(window: window, navigationController: navigationController)
    }

    func migrate() {
        dbMigationManager.migrateIfNeeded()
    }

    func configureRootScene() {
        window.rootViewController = navigationController
        applicationNavigator.goTo(scene: .searchScreen)
    }

    private func setupApplicationNavigator (window: UIWindow, navigationController: UINavigationController) {
      let navigator = ApplicationNavigatorImpl(window: window, navigationController: navigationController)
      navigationLocator.populate(navigator: navigator)
    }

}

class ApplicationAssemblyFactory{
  static func  defaultAssembly(window: UIWindow) -> ApplicationAssembly {
    return ApplicationAssemblyImpl(window: window, dataBaseMigrationManager: DatabaseMigrationManagerFactory().defaultManager(), navigationLocator: ApplicationNavigatorLocator())
  }
}

