//
//  ApplicationAssembly.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit

protocol ApplicationAssembly {
  var applicationNavigator: ApplicationNavigator { get }
  func migrate()
  func configureRootScene()

}
