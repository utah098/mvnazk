//
//  AppDelegate.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 3/3/20.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var applicationAssembly: ApplicationAssembly!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        applicationAssembly = ApplicationAssemblyFactory.defaultAssembly(window: window)
        applicationAssembly.migrate()
        applicationAssembly.configureRootScene()
        self.window = window
        return true
    }

}

