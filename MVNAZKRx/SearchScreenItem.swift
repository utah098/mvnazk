//
//  SearchScreenItem.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 09.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

enum SearchScreenItem {
    case account(model: AccountModel)
    case loading
}
