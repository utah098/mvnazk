//
//  LoadingCell.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 09.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SnapKit

class LoadingCell: UITableViewCell {

    var activityIndicator: UIActivityIndicatorView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setupSubviews() {
        selectionStyle = .none
        contentView.backgroundColor = .white
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.color = .blue
        indicator.hidesWhenStopped = true
        contentView.addSubview(indicator)
        indicator.snp.makeConstraints {
            $0.top.equalTo(self).offset(10)
            $0.bottom.equalTo(self).offset(-10)
            $0.centerX.equalTo(self)
        }
        indicator.startAnimating()
    }

}
