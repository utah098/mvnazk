//
//  IconTableViewCell.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import RxSwift

class IconTableViewCell: UITableViewCell {

    let htmlButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "html"), for: .normal)
        return btn
    }()

    let pdfButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "png"), for: .normal)
        return btn
    }()

    var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }

    func configCell(with pdfLink: String?) {
        pdfButton.isHidden = pdfLink == nil
    }

    func configureUI() {
        backgroundColor = .white
        selectionStyle = .none
        addSubview(htmlButton)
        addSubview(pdfButton)

        htmlButton.snp.makeConstraints {
            $0.top.equalTo(self).offset(4)
            $0.left.equalTo(self).offset(16)
            $0.bottom.equalTo(self).offset(-4)
            $0.width.height.equalTo(64)
        }

        pdfButton.snp.makeConstraints {
            $0.top.equalTo(self).offset(4)
            $0.left.equalTo(htmlButton.snp.right).offset(10)
            $0.bottom.equalTo(self).offset(-4)
            $0.width.height.equalTo(64)
        }

    }

}
