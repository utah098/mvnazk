//
//  AccountDetailRootView.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 09.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SnapKit

class AccountDetailRootView: BaseView {

    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.backgroundColor = .white
        tbl.alwaysBounceVertical = true
        tbl.separatorStyle = .none
        tbl.allowsSelection = false
        tbl.register(cellType: NameTableViewCell.self)
        tbl.register(cellType: WorkTableViewCell.self)
        tbl.register(cellType: IconTableViewCell.self)
        return tbl
    }()

    let refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = .black
        return refresh
    }()

}

extension AccountDetailRootView {

    func configureConstraints() {
        backgroundColor = .white
        addSubview(tableView)

        tableView.snp.makeConstraints {
            $0.top.equalTo(safeAreaTop)
            $0.leading.equalTo(self)
            $0.trailing.equalTo(self)
            $0.bottom.equalTo(safeAreaBottom)
        }

        tableView.addSubview(refreshControl)
    }
}
