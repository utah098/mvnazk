//
//  BaseView.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit

class BaseView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    func sharedInit() {}
}

protocol HasCustomView {
    associatedtype RootView: UIView
}

extension HasCustomView where Self: UIViewController {
    var rootView: RootView {
        guard let customView = view as? RootView else {
            fatalError("Expected view to be of type \(RootView.self) but got \(type(of: view)) instead")
        }
        return customView
    }
}
