//
//  FavoriteAccountsViewController.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 14.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import RxSwift

class FavoriteAccountsViewController: BaseViewController, HasCustomView {
    typealias RootView = FavoriteAccountsRootView

    var viewModel: FavoriteAccountsViewModel

    private let disposeBag = DisposeBag()

    init(viewModel: FavoriteAccountsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = FavoriteAccountsRootView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBinding()
        rootView.configureConstraints()
        navigationItem.title = "Сохраненные Аккаунты"
    }

    func setupBinding() {
        rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .take(1)
            .mapToVoid()
            .bind(to: viewModel.loadTrigger)
            .disposed(by: disposeBag)


        viewModel.items.asDriver()
            .drive(rootView.tableView.rx.items(cellIdentifier: "AccountTableViewCell", cellType: AccountTableViewCell.self)) { index, model, cell in
                cell.nameLabel.text = model.firstname + " " + model.lastname
                cell.workLabel.text = model.placeOfWork
            }
        .disposed(by: disposeBag)
    }

}

