//
//  Observable+Drive.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension ObservableType {

    func mapToVoid() -> Observable<Void> {
        return map { _ in }
    }

    func asDriverOnErrorJustComplete() -> Driver<E> {
      return asDriver { _ in
        return Driver.empty()
      }
    }
}
