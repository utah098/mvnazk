//
//  AccountTableViewCell.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.08.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SnapKit

class AccountTableViewCell: UITableViewCell {

    static var reuseID: String = "AccountTableViewCell"

    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()

    let nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .init(red: 0.239, green: 0.357, blue: 0.608, alpha: 1)
        lbl.font = .systemFont(ofSize: 17, weight: .medium)
        lbl.numberOfLines = 0
        return lbl
    }()

    let workLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.font = .systemFont(ofSize: 14)
        lbl.numberOfLines = 0
        return lbl
    }()

    let htmlIV: UIImageView = {
        let iV = UIImageView()
        iV.image = UIImage(named: "html")
        return iV
    }()

    let pdfIV: UIImageView = {
        let iV = UIImageView()
        iV.image = UIImage(named: "png")
        return iV
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }

    func configureUI() {
        backgroundColor = .white
        selectionStyle = .none
        addSubview(containerView)
        containerView.addSubview(workLabel)
        containerView.addSubview(nameLabel)
        containerView.addSubview(htmlIV)
        containerView.addSubview(pdfIV)

        containerView.snp.makeConstraints {
            $0.top.equalTo(self).offset(4)
            $0.left.equalTo(self).offset(16)
            $0.bottom.equalTo(self).offset(-4)
            $0.right.equalTo(self).offset(-16)
        }

        nameLabel.snp.makeConstraints {
            $0.top.equalTo(containerView.snp.top).offset(10)
            $0.left.equalTo(containerView.snp.left).offset(10)
            $0.right.equalTo(containerView.snp.right).offset(-10)
        }

        workLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(5)
            $0.left.equalTo(containerView.snp.left).offset(10)
            $0.right.equalTo(containerView.snp.right).offset(-10)
        }

        htmlIV.snp.makeConstraints {
            $0.top.equalTo(workLabel.snp.bottom).offset(5)
            $0.left.equalTo(containerView.snp.left).offset(15)
            $0.bottom.equalTo(containerView.snp.bottom).offset(-15)
            $0.width.height.equalTo(30)
        }

        pdfIV.snp.makeConstraints {
            $0.top.equalTo(workLabel.snp.bottom).offset(5)
            $0.left.equalTo(htmlIV.snp.right).offset(10)
            $0.bottom.equalTo(containerView.snp.bottom).offset(-15)
            $0.width.height.equalTo(30)
        }

    }

    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        workLabel.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
