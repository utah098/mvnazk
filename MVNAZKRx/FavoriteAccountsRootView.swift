//
//  FavoriteAccountsRootView.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 14.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SnapKit

class FavoriteAccountsRootView: BaseView {

    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.backgroundColor = .white
        tbl.alwaysBounceVertical = true
        tbl.register(cellType: AccountTableViewCell.self)
        return tbl
    }()

}

extension FavoriteAccountsRootView {

    func configureConstraints() {
        backgroundColor = .white
        addSubview(tableView)

        tableView.snp.makeConstraints {
            $0.top.equalTo(safeAreaTop)
            $0.leading.equalTo(self)
            $0.trailing.equalTo(self)
            $0.bottom.equalTo(safeAreaBottom)
        }

    }
}
