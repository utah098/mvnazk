//
//  UIScrollView+Rx.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.08.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import RxSwift
import UIKit


extension UIScrollView {
    var rx_reachedBottom: Observable<Void> {
        return rx.contentOffset
            .flatMap { [weak self] contentOffset -> Observable<Void> in
                guard let scrollView = self else {
                    return Observable.empty()
                }

                let visibleHeight = scrollView.frame.height - scrollView.contentInset.top - scrollView.contentInset.bottom
                let y = contentOffset.y + scrollView.contentInset.top
                let threshold = max(0.0, scrollView.contentSize.height - visibleHeight)

                return y > threshold ? Observable.just(()) : Observable.empty()
        }
    }
}
