//
//  UIView+Extensions.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }

    var safeAreaTop: ConstraintItem {
        guard #available(iOS 11, *) else {
            return self.parentViewController?.topLayoutGuide.snp.bottom ?? snp.top
        }
        return  self.safeAreaLayoutGuide.snp.top
    }

    var safeAreaBottom: ConstraintItem {
        guard #available(iOS 11, *) else {
            return self.parentViewController?.bottomLayoutGuide.snp.top ?? snp.bottom
        }
        return safeAreaLayoutGuide.snp.bottom
    }
}
