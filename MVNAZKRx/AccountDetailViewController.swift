//
//  AccountDetailViewController.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 09.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import RxSwift

class AccountDetailViewController: BaseViewController, HasCustomView {
    typealias RootView = AccountDetailRootView

    var viewModel: AccountDetailViewModel

    private let disposeBag = DisposeBag()

    init(viewModel: AccountDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = AccountDetailRootView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        rootView.configureConstraints()
        setupBinding()
        navigationItem.title = "Детальная Информация"
    }

    func setupBinding() {
        rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .take(1)
            .mapToVoid()
            .bind(to: viewModel.loadTrigger)
            .disposed(by: disposeBag)

        rootView.refreshControl.rx
            .controlEvent(.valueChanged)
            .bind(to: self.viewModel.refreshTrigger)
            .disposed(by: disposeBag)

        viewModel.items.asDriver()
            .drive(rootView.tableView.rx.items) { (tv, row, item) -> UITableViewCell in
                switch item {
                case .name( let text):
                    let cell = tv.dequeueReusableCell(ofType: NameTableViewCell.self, at: IndexPath(row: row, section: 0))
                    cell.nameLabel.text = text
                    return cell
                case .work( let text):
                    let cell = tv.dequeueReusableCell(ofType: WorkTableViewCell.self, at: IndexPath(row: row, section: 0))
                    cell.nameLabel.text = text
                    return cell
                case .file( let acc):
                    let cell = tv.dequeueReusableCell(ofType: IconTableViewCell.self, at: IndexPath(row: row, section: 0))
                    cell.configCell(with: acc.linkPDF)
                    cell.htmlButton.rx.tap
                        .map { return acc  }
                        .bind(to: self.viewModel.htmlTapped)
                        .disposed(by: cell.disposeBag)
                    
                    return cell
                }
        }
        .disposed(by: disposeBag)

        addRightBarButton()

        self.viewModel.activityIndicator.asDriver()
            .drive(rootView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        self.viewModel.errorSubject
            .asDriverOnErrorJustComplete()
            .drive(self.rx.showErrorAlert)
            .disposed(by: disposeBag)


        self.viewModel.itemSaved
            .asDriverOnErrorJustComplete()
            .drive(onNext: {

                self.showAlert(title: "Добавлено в избранное", message: nil, style: .alert,
                               actions: [AlertAction.action(title: "OK", style: .cancel)]
                ).subscribe(onNext: { selectedIndex in
                    print(selectedIndex)
                }).disposed(by: self.disposeBag)

            }).disposed(by: disposeBag)

        self.viewModel.htmlSelected
        .mapToVoid()
        .asDriverOnErrorJustComplete()
        .drive()
        .disposed(by: disposeBag)

        self.viewModel.pdfSelected
        .mapToVoid()
        .asDriverOnErrorJustComplete()
        .drive()
        .disposed(by: disposeBag)
    }

    private func addRightBarButton() {
        let rightButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
        navigationItem.rightBarButtonItem = rightButton
        rightButton.rx.tap
            .mapToVoid()
            .bind(to: viewModel.addTrigger)
            .disposed(by: disposeBag)
    }

}
