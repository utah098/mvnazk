//
//  AccountModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 31.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class AccountModel: Object, Codable {
    
    dynamic var id: String
    dynamic var firstname: String
    dynamic var lastname: String
    dynamic var placeOfWork: String?
    dynamic var position: String?
    dynamic var linkPDF: String?

    override class func primaryKey() -> String? {
      return "id"
    }
}
