//
//  DetailModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 09.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

struct DetailModel: Codable {

    let firstname: String
    let lastname: String
    let middlename: String
    let workPlace: String?
    let responsiblePosition: String?
    let workPost: String?
    let postType: String?
    let linkPDF: String?
}
