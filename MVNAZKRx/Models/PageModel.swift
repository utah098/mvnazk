//
//  PageModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 31.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

struct PageModel: Codable {

  let currentPage: Int
  let batchSize: Int
  let totalItems: String
}
