//
//  Rx+Codable.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 31.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RxSwift

extension ObservableType {

  public func mapObject<T: Codable>(type: T.Type) -> Observable<T> {
    return flatMap { data -> Observable<T> in
      let responseTuple = data as? (HTTPURLResponse, Data)

      guard let jsonData = responseTuple?.1 else {
        throw DomainError.parsingError
      }

      let decoder = JSONDecoder()
      let object = try decoder.decode(T.self, from: jsonData)
      return Observable.just(object)
    }
  }

    public func mapJSONObject<T: Codable>(type: T.Type) -> Observable<T> {
      return flatMap { data -> Observable<T> in
        let responseTuple = data as? [String: Any]

        guard let jsonData = responseTuple else {
          throw DomainError.parsingError
        }

        guard let detailData = try? JSONSerialization.data(withJSONObject: jsonData, options: .fragmentsAllowed) else {
            throw DomainError.parsingError
        }
        let decoder = JSONDecoder()
        let model = try decoder.decode(T.self, from: detailData)
        return Observable.just(model)
      }
    }

  public func mapArray<T: Codable>(type: T.Type) -> Observable<[T]> {
    return flatMap { data -> Observable<[T]> in
      let responseTuple = data as? Response

      guard let jsonData = responseTuple?.1 else {
        throw DomainError.parsingError
      }

      let decoder = JSONDecoder()
      let objects = try decoder.decode([T].self, from: jsonData)
      return Observable.just(objects)
    }
  }
}
