//
//  SearchResponseModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 26.08.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

struct SearchResponseModel: Codable {

  let page: PageModel
  let items: [AccountModel]

}
