//
//  DomainError.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 31.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

enum DomainError: Error {
  case notFound
  case parsingError
  case networkError
  case entitieNotFound
}
