//
//  SearchViewModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

final class SearchViewModel {

    private let accountRepo: AccountRepo
    private let navigator: ApplicationNavigator

    // @INPUT
    let refreshTrigger = PublishSubject<Void>()
    let inputText = PublishSubject<String>()
    let loadNextPageTrigger = PublishSubject<Void>()
    let selectedTrigger = PublishSubject<IndexPath>()
    let favoriteTrigger = PublishSubject<Void>()
    let loading = BehaviorRelay<Bool>(value: false)

    // @OUTPUT
    let activityIndicator = ActivityIndicator()
    let errorSubject = PublishSubject<Error>()
    let items = BehaviorRelay<[SearchScreenItem]>(value: [])
    let selectedItem = PublishSubject<SearchScreenItem>()
    let favoriteTapped = PublishSubject<Void>()

    var pageIndex: Int = 1

    private let disposeBag = DisposeBag()

    init(accountRepo: AccountRepo, appNavigator: ApplicationNavigator) {
        self.accountRepo = accountRepo
        self.navigator = appNavigator

        let refreshRequest = loading.asObservable()
            .sample(refreshTrigger)
            .flatMap { loading -> Observable<Int> in
                if loading {
                    return Observable.empty()
                } else {
                    return Observable<Int>.create { observer in
                        self.pageIndex = 1
                        print("reset page index to 0")
                        observer.onNext(1)
                        observer.onCompleted()
                        return Disposables.create()
                    }
                }
        }
        .debug("refreshRequest", trimOutput: false)

        let inputRequest = loading.asObservable()
            .sample(inputText)
            .flatMap { loading -> Observable<Int> in
                if loading {
                    return Observable.empty()
                } else {
                    return Observable<Int>.create { observer in
                        self.pageIndex = 1
                        print("reset page index to 0")
                        observer.onNext(1)
                        observer.onCompleted()
                        return Disposables.create()
                    }
                }
        }
        .debug("inputRequest", trimOutput: false)

        let nextPageRequest = loading.asObservable()
            .sample(loadNextPageTrigger)
            .flatMap { loading -> Observable<Int> in
                if loading {
                    return Observable.empty()
                } else {
                    return Observable.create { observer in
                        self.pageIndex += 1
                        print("New page is \(self.pageIndex)")
                        observer.onNext(self.pageIndex)
                        observer.onCompleted()
                        return Disposables.create()
                    }
                }
        }
        .debug("nextPageRequest", trimOutput: false)

        let request = Observable.merge(refreshRequest, nextPageRequest, inputRequest)
            .share(replay: 1)
            .debug("request", trimOutput: false)

        let response = request
            .withLatestFrom(inputText) { (page, string) in
                return (page, string)
            }
            .flatMapLatest { (page, text) in
                return accountRepo.getAccounts(page: page, name: text)
                    .catchError { (error) -> Observable<[AccountModel]> in
                        self.errorSubject.onNext(error)
                        return Observable.just([])
                }
                .trackActivity(self.activityIndicator)
            }
            .share(replay: 1)

            .debug("response", trimOutput: false)

        Observable
            .combineLatest( loading ,response, items.asObservable()) { load ,res, it in
                let newItems = res.map { SearchScreenItem.account(model: $0)}
                return self.pageIndex == 1 ? newItems : load ? (it + newItems + [SearchScreenItem.loading]) : it + newItems
            }
            .sample(response)
            .bind(to: items)
            .disposed(by: disposeBag)

        Observable
               .merge(request.map{ _ in true },
                   response.map { _ in false },
                   errorSubject.map { _ in false })
               .bind(to: loading)
               .disposed(by: disposeBag)

        selectedTrigger.asObservable()
            .withLatestFrom(items) { (indexPath, items) in
                return items[indexPath.row]
            }
            .do(onNext: { account in
                switch account {
                case .account(model: let item):
                    self.navigator.goTo(scene: .searchResult(acc: item))
                case .loading:
                    break
                }

            })
            .bind(to: selectedItem)
            .disposed(by: disposeBag)

        favoriteTrigger.asObservable()
            .do(onNext: { _ in
                self.navigator.goTo(scene: .favorites)
            })
        .bind(to: favoriteTapped)
        .disposed(by: disposeBag)

    }
}
