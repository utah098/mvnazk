//
//  EntitiesDatabaseManager.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

typealias FilterWhere = String

class EntitiesDatabaseManager <T>  {

    func saveEntities(entities: [T]) -> Observable<Void> {
        exeption()
    }

    func saveEntitie(entitie: T) -> Observable<Void> {
        exeption()
    }

    func fetchAllEntities(filter: FilterWhere?) -> Observable<[T]> {
        exeption()
    }

    func fetchFirstEntity(filter: FilterWhere?) -> Observable<T> {
        exeption()
    }
}

func exeption() -> Never {
    fatalError("Abstract function, please override it")
}
