//
//  DatabaseMigrationManager.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

protocol DatabaseMigrationManager {
  func migrateIfNeeded()
}
