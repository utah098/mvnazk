//
//  DatabaseMigrationManagerImpl.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseMigrationManagerImpl: DatabaseMigrationManager {
  func migrateIfNeeded() {
    let config = Realm.Configuration(schemaVersion: 1)
    Realm.Configuration.defaultConfiguration = config
  }
}

class DatabaseMigrationManagerFactory {
  func defaultManager() -> DatabaseMigrationManager {
    return DatabaseMigrationManagerImpl()
  }
}
