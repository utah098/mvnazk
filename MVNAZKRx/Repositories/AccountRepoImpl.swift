//
//  AccountRepoImpl.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 19.06.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RxSwift

final class AccountRepoImpl: AccountRepo {


    private let apiClient: ApiClient
    private let entitiesDatabaseManager: EntitiesDatabaseManager<AccountModel>

    init(apiClient: ApiClient, entitiesDatabaseManager: EntitiesDatabaseManager<AccountModel>) {
      self.apiClient = apiClient
      self.entitiesDatabaseManager = entitiesDatabaseManager
    }

    func getAccounts(page: Int, name: String) -> Observable<[AccountModel]> {
        return apiClient
            .sendRequest(router: APIRouter.searchDeclarations(page: page, name: name))
            .mapObject(type: SearchResponseModel.self)
            .map { $0.items }
    }

    func getDeclaration(id: String) -> Observable<DetailModel> {
        return apiClient
            .sendRequest(router: APIRouter.getDeclaration(id: id))
            .map({ (url, data) in
                guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
                  let result = jsonObject as? [String: Any], let data = result["data"] as? [String : Any], let step = data["step_1"] as? [String : Any] else {
                    throw DomainError.parsingError
                }

                guard let detailData = try? JSONSerialization.data(withJSONObject: step, options: .fragmentsAllowed) else {
                    throw DomainError.parsingError
                }
                let decoder = JSONDecoder()
                let model = try decoder.decode(DetailModel.self, from: detailData)
                return model
            })
    }

    func saveAccToFavorite(model: AccountModel) -> Observable<Void> {
        return entitiesDatabaseManager.saveEntitie(entitie: model)
    }

    func getSavedDecrations() -> Observable<[AccountModel]> {
        return entitiesDatabaseManager.fetchAllEntities(filter: nil)
    }

    func getSafariLink(id: String) -> Observable<String> {
        return Observable.create { observer in
            observer.onNext(APIRouter.getSafariVC.baseURL + id)
            observer.onCompleted()
            return Disposables.create()
        }
    }

}

class AccountRepoFactory {
  static func defaultRepo() -> AccountRepo {
    return AccountRepoImpl(
        apiClient: ApiClientFactory().defaultApiClient(), entitiesDatabaseManager: EntitiesDatabaseManagerFactory.defaultManager())
  }
}
