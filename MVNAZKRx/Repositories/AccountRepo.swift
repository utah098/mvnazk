//
//  AccountRepo.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 19.06.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RxSwift

protocol AccountRepo {
    func getAccounts(page: Int, name: String) -> Observable <[AccountModel]>
    func getDeclaration(id: String) -> Observable<DetailModel>
    func saveAccToFavorite(model: AccountModel) -> Observable<Void>
    func getSavedDecrations() -> Observable<[AccountModel]>
    func getSafariLink(id: String) -> Observable<String>
}
