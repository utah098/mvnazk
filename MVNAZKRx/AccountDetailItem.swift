//
//  AccountDetailItem.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

enum AccountDetailItem {
    case name(_ text: String)
    case work(_ text: String)
    case file(_ link: AccountModel)
}
