//
//  WorkTableViewCell.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 13.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit

class WorkTableViewCell: UITableViewCell {

    lazy var nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .italicSystemFont(ofSize: 17)
        lbl.textColor = .black
        lbl.numberOfLines = 0
        return lbl
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }

    func configureUI() {
        backgroundColor = .white
        selectionStyle = .none
        addSubview(nameLabel)

        nameLabel.snp.makeConstraints {
            $0.top.equalTo(self).offset(4)
            $0.left.equalTo(self).offset(16)
            $0.bottom.equalTo(self).offset(-4)
            $0.right.equalTo(self).offset(-16)
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
