//
//  TargetType.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import Alamofire

protocol TargetType {
  var baseURL: String { get }
  var path: String { get }
  var method: HTTPMethod { get }
  var parameters: Parameters? { get }
  var encoding: ParameterEncoding { get }
  var header: [String: String]? { get }
}
