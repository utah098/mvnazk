//
//  ApiRouter.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter {
    case searchDeclarations(page: Int, name: String)
    case getDeclaration(id: String)
    case getSafariVC
}

extension APIRouter: TargetType {

    var baseURL: String {
        switch self {
        case .getSafariVC:
            return "https://public.nazk.gov.ua/declaration/"
        default:
            return "https://public-api.nazk.gov.ua"
        }
    }

    var path: String {
        switch self {
        case .searchDeclarations:
            return "/v1/declaration"
        case .getDeclaration(id: let id):
            return "/v1/declaration/\(id)"
        default:
            return " "
        }
    }

    var method: HTTPMethod {
        switch self {
        case .searchDeclarations, .getDeclaration:
            return .get
        default:
            return .post
        }
    }

    var parameters: [String : Any]? {
        switch self {
        case .searchDeclarations(let page, let name):
            return ["page": page, "q": name]
        default:
            return nil
        }
    }

    var encoding: ParameterEncoding {
        return URLEncoding()
    }

    var header: [String : String]? {
        return nil
    }


}
