//
//  ApiClient.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation
import RxSwift

typealias Response = (HTTPURLResponse, Data)

protocol ApiClient {
  func sendRequest(router: TargetType) -> Observable <Response>
}
