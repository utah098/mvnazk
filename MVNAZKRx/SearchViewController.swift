//
//  ViewController.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 3/3/20.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa

class SearchViewController: BaseViewController, HasCustomView {
    typealias RootView = SearchRootView

    var viewModel: SearchViewModel

    private let startLoadingOffset: CGFloat = 20.0
    private let disposeBag = DisposeBag()

    override func loadView() {
        view = SearchRootView()
    }

    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Поиск Деклараций"
        rootView.configureConstraints()
        setupBinding()
        hideKeyboardWhenTappedOnTableView()
    }

    func hideKeyboardWhenTappedOnTableView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        rootView.tableView.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        rootView.endEditing(true)
    }


    func setupBinding() {

        rootView.refreshControl.rx
            .controlEvent(.valueChanged)
            .bind(to: self.viewModel.refreshTrigger)
            .disposed(by: disposeBag)

        rootView.tableView.rx
            .itemSelected
            .bind(to: self.viewModel.selectedTrigger)
            .disposed(by: disposeBag)

        rootView.searchBar.rx
            .text.orEmpty
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .bind(to: self.viewModel.inputText)
            .disposed(by: disposeBag)

        rootView.tableView.rx_reachedBottom
            .bind(to: self.viewModel.loadNextPageTrigger)
            .disposed(by: disposeBag)

        addRightBarButton()

        self.viewModel.items.asDriver()
            .drive(rootView.tableView.rx.items) { (tv, row, item) -> UITableViewCell in
                switch item {
                case .account(model: let model):
                    let cell = tv.dequeueReusableCell(ofType: AccountTableViewCell.self, at: IndexPath(row: row, section: 0))
                    cell.nameLabel.text = model.firstname + " " + model.lastname
                    cell.workLabel.text = model.placeOfWork
                    return cell
                case .loading:
                    return LoadingCell(style: .default, reuseIdentifier: "loading")
                }
        }
        .disposed(by: disposeBag)

        rootView.tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)


        self.viewModel.activityIndicator.asDriver()
            .drive(onNext: { isRefreshing in
                if isRefreshing {
                    self.rootView.refreshControl.beginRefreshing()
                    self.rootView.tableView.contentOffset = CGPoint(x: 0, y: -self.rootView.refreshControl.frame.height)
                } else {
                    self.rootView.refreshControl.endRefreshing()
                }
            })
//        .drive(rootView.refreshControl.rx.isRefreshing)
        .disposed(by: disposeBag)

        self.viewModel.errorSubject
            .asDriverOnErrorJustComplete()
            .drive(onNext: { err in
                self.showAlert(title: err.localizedDescription, message: nil, style: .alert,
                               actions: [AlertAction.action(title: "OK", style: .cancel)]
                ).subscribe(onNext: { selectedIndex in
                    print(selectedIndex)
                }).disposed(by: self.disposeBag)

            })
            .disposed(by: disposeBag)

        self.viewModel.selectedItem
            .mapToVoid()
            .asDriverOnErrorJustComplete()
            .drive()
            .disposed(by: disposeBag)

        self.viewModel.favoriteTapped
            .asDriverOnErrorJustComplete()
            .drive()
            .disposed(by: disposeBag)
    }

    private func addRightBarButton() {
        let rightButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: nil)
        navigationItem.rightBarButtonItem = rightButton
        rightButton.rx.tap
            .mapToVoid()
            .bind(to: viewModel.favoriteTrigger)
            .disposed(by: disposeBag)
    }
}

extension SearchViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        rootView.searchBar.resignFirstResponder()
    }
}

