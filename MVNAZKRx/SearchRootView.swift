//
//  SearchRootView.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SnapKit

class SearchRootView: BaseView {

    let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.backgroundColor = .lightGray
        searchBar.placeholder = "Search"
//        textField.textColor = .black
//        textField.backgroundColor = .white
        return searchBar
    }()

//    let searchButton: UIButton = {
//        let btn = UIButton()
//        btn.setTitle("Start", for: .normal)
//        return btn
//    }()

    let refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = .black
        return refresh
    }()

    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.backgroundColor = .white
        tbl.alwaysBounceVertical = true
        tbl.register(cellType: AccountTableViewCell.self)
        return tbl
    }()

}

extension SearchRootView {

    func configureConstraints() {
        backgroundColor = .white
        addSubview(searchBar)
        addSubview(tableView)

        searchBar.snp.makeConstraints {
            $0.top.equalTo(safeAreaTop)
            $0.leading.equalTo(self)
            $0.trailing.equalTo(self)
        }
        tableView.snp.makeConstraints {
            $0.top.equalTo(searchBar.snp.bottom)
            $0.leading.trailing.equalTo(self)
            $0.bottom.equalTo(safeAreaBottom)
        }

        tableView.addSubview(refreshControl)
    }
}
