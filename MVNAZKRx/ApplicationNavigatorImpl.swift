//
//  ApplicationNavigatorImpl.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit
import SafariServices

final class ApplicationNavigatorImpl: ApplicationNavigator {

    let window: UIWindow
    let navigationController: UINavigationController

    init(window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = navigationController
    }

    func goTo(scene: ApplicationScene) {
        switch scene {
        case .searchScreen:
            navigateToSearch()
        case .searchResult(let acc):
            navigateToSearchResult(acc: acc)
        case .favorites:
            openFavorites()
        case .safariVC(let link):
            openSafariVC(link: link)
        }
    }

    func navigateToSearch() {
        let vm = SearchViewModel(accountRepo: AccountRepoFactory.defaultRepo(), appNavigator: self)
        let vc = SearchViewController(viewModel: vm)
        self.navigationController.pushViewController(vc, animated: true)
    }

    func navigateToSearchResult(acc: AccountModel) {
        let vm = AccountDetailViewModel(account: acc, accountRepo: AccountRepoFactory.defaultRepo(), appNavigator: self)
        let vc = AccountDetailViewController(viewModel: vm)
        self.navigationController.pushViewController(vc, animated: false)
    }

    func openFavorites() {
        let vm = FavoriteAccountsViewModel(accountRepo: AccountRepoFactory.defaultRepo(), appNavigator: self)
        let vc = FavoriteAccountsViewController(viewModel: vm)
        self.navigationController.pushViewController(vc, animated: true)
    }

    func openSafariVC(link: String) {
        if let reportUrl = URL(string: link) {
            let safariVC = SFSafariViewController(url: reportUrl)
//            safariVC.delegate = self
            self.navigationController.present(safariVC, animated: true, completion: nil)
        }
    }
}

//extension ApplicationNavigatorImpl: SFSafariViewControllerDelegate {
//    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
//        self.navigationController.dismiss(animated: true)
//    }
//}

