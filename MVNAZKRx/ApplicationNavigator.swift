//
//  ApplicationNavigator.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import UIKit

enum ApplicationScene {
    case searchScreen
    case searchResult(acc: AccountModel)
    case favorites
    case safariVC(link: String)
}

protocol ApplicationNavigator {
    func goTo(scene: ApplicationScene)
}
