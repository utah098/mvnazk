//
//  ApplicationNavigatorLocator.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 27.05.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import Foundation

class ApplicationNavigatorLocator {
    private static var applicationNavigator: ApplicationNavigator?

    func populate(navigator: ApplicationNavigator) {
      ApplicationNavigatorLocator.applicationNavigator = navigator
    }

    var shared: ApplicationNavigator {
      guard let  applicationNavigator = ApplicationNavigatorLocator.applicationNavigator else {
        //Populate first
        fatalError()
      }
      return applicationNavigator
    }
}
