//
//  FavoriteAccountsViewModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 14.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import RxSwift
import RxCocoa

final class FavoriteAccountsViewModel {

    private let accountRepo: AccountRepo
    private let navigator: ApplicationNavigator

    // @INPUT
    let loadTrigger = PublishSubject<Void>()

    // @OUTPUT
    let items = BehaviorRelay<[AccountModel]>(value: [])

    private let disposeBag = DisposeBag()

    init(accountRepo: AccountRepo, appNavigator: ApplicationNavigator) {
        self.accountRepo = accountRepo
        self.navigator = appNavigator

        loadTrigger.asObservable()
            .flatMapLatest { _ in
                return accountRepo.getSavedDecrations()
        }
        .bind(to: items)
        .disposed(by: disposeBag)

    }

}
