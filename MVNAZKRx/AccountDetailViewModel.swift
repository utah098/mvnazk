//
//  AccountDetailViewModel.swift
//  MVNAZKRx
//
//  Created by Николай Войтович on 09.09.2020.
//  Copyright © 2020 Николай Войтович. All rights reserved.
//

import RxSwift
import RxCocoa

final class AccountDetailViewModel {

    private let accountRepo: AccountRepo
    private let navigator: ApplicationNavigator
    private let account: AccountModel

    // @INPUT
    let loadTrigger = PublishSubject<Void>()
    let refreshTrigger = PublishSubject<Void>()
    let addTrigger = PublishSubject<Void>()
    let htmlTapped = PublishSubject<AccountModel>()
    let pdfTapped = PublishSubject<AccountModel>()

    // @OUTPUT
    let activityIndicator = ActivityIndicator()
    let errorSubject = PublishSubject<Error>()
    let items = BehaviorRelay<[AccountDetailItem]>(value: [])
    let itemSaved = PublishSubject<Void>()
    let htmlSelected = PublishSubject<String>()
    let pdfSelected = PublishSubject<AccountModel>()

    private let disposeBag = DisposeBag()

    init(account: AccountModel,accountRepo: AccountRepo, appNavigator: ApplicationNavigator) {
        self.accountRepo = accountRepo
        self.navigator = appNavigator
        self.account = account

//        refreshTrigger.asObservable()
//            .flatMapLatest { _ in
//                return accountRepo.getDeclaration(id: self.account.id)
//                    .catchError { (error) -> Observable<DetailModel> in
//                        self.errorSubject.onNext(error)
//                        return Observable.empty()
//                }
//                .trackActivity(self.activityIndicator)
//        }
//        .map{ self.buildItemsModel(acc: account, detail: $0) }
//        .bind(to: items)
//        .disposed(by: disposeBag)
        Observable.merge(refreshTrigger.asObservable(), loadTrigger.asObservable())
        .flatMapLatest { _ in
                return accountRepo.getDeclaration(id: self.account.id)
                    .catchError { (error) -> Observable<DetailModel> in
                        self.errorSubject.onNext(error)
                        return Observable.empty()
                }
                .trackActivity(self.activityIndicator)
        }
        .map{ self.buildItemsModel(acc: account, detail: $0) }
        .bind(to: items)
        .disposed(by: disposeBag)

        addTrigger.asObservable()
            .flatMapLatest { _ in
                return accountRepo.saveAccToFavorite(model: account)
        }
        .bind(to: itemSaved)
        .disposed(by: disposeBag)

        htmlTapped.asObservable()
            .flatMapLatest{ acc -> Observable<String> in
                return accountRepo.getSafariLink(id: acc.id)
            }
        .do(onNext: { link in
            self.navigator.goTo(scene: .safariVC(link: link))
        })
        .bind(to: htmlSelected)
        .disposed(by: disposeBag)

        pdfTapped.asObservable()
            .do(onNext: { (acc) in
                guard let link = acc.linkPDF else {
                    return
                }
                 self.navigator.goTo(scene: .safariVC(link: link))
            })
            .bind(to: pdfSelected)
            .disposed(by: disposeBag)
        
    }


    private func buildItemsModel(acc: AccountModel,
                                 detail: DetailModel) -> [AccountDetailItem] {
      return [
        AccountDetailItem.name(detail.lastname),
        AccountDetailItem.name(detail.firstname),
        AccountDetailItem.name(detail.middlename),
        AccountDetailItem.work(detail.postType ?? "[Конфіденційна інформація]"),
        AccountDetailItem.work(detail.workPost ?? "[Конфіденційна інформація]"),
        AccountDetailItem.work(detail.workPlace ?? "[Конфіденційна інформація]"),
        AccountDetailItem.work(detail.responsiblePosition ?? "[Конфіденційна інформація]"),
        AccountDetailItem.file(acc)

      ]
    }
}
